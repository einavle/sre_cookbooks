# Full CI/CD for SRE_TEST

This project contains the source code for the SER_TEST deployment

### Follows are instructions to trigger a CI/CD cycle
 1. clone the project ```sre_test``` (the other project I shared you with), make some changes, commit and push to origin master
 2. login to jenkins at http://ec2-18-219-171-230.us-east-2.compute.amazonaws.com:8080/view/sre/ with credential: mayal,maya080409 and wait untill your pipeline that was triggered by gitlab will be finished with SUCCESS.
 3. login with ssh to our production host:<br>
  ```ssh  ec2-user@ec2-18-222-231-110.us-east-2.compute.amazonaws.com``` password:12345<br>
  type: ```sudo docker ps``` and verify that the version (under the IMAGE column) is corelated with your last jenkins build version.






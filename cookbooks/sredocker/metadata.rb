# frozen_string_literal: true
name 'sredocker'
recipe "sredocker::default", "default recipe"
depends 'docker'
description 'deploy docker  image'
maintainer 'Einav Leyboshor'
maintainer_email 'enav.le@gmail.com'
license 'Apache-2.0'
version '1.0.0'